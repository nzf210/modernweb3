import React, { useEffect, useState } from "react";
import { ethers } from "ethers";
import { contractABI, contarctAddress } from "../utils/constants";
export const TransactionContext = React.createContext();
const { ethereum } = window;

const getEthereumContract = () => {
    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();
    const transactionContract = new ethers.Contract(
        contarctAddress,
        contractABI,
        signer
    );

    console.log({
        provider,
        signer,
        transactionContract
    });

    return transactionContract;
};

export const TransactionProvider = ({ children }) => {
    const [currentAccount, setCurrentAccount] = useState();
    const [transactions, setTransactions] = useState([]);
    const [formData, setFormData] = useState({
        addressTo: '', amount: '', keyword: '', message: ''
    });
    const [isLoading, setIsLoading] = useState(false);
    const [transactionCount, setTransactionCount] = useState(localStorage.getItem('transactionCount'));

    const handleChange = (e, name) => {
        setFormData((prevSate) => ({
            ...prevSate, [name]: e.target.value
        }));
    }

    const getAllTransactions = async () => {
        try {
            if (!ethereum) return alert("Please Install Metamask");
            const trxContarct = getEthereumContract();
            const availableTransactions = await trxContarct.getAllTransactions();
            const structuredTrx = availableTransactions.map((trx) => ({
                addressTo: trx.receiver,
                addressFrom: trx.sender,
                timestamp: new Date(trx.timestamp.toNumber() * 1000).toLocaleString(),
                message: trx.message,
                keyword: trx.keyword,
                amount: parseInt(trx.amount._hex) / (10 ** 18)
            }));
            console.log(structuredTrx);
            setTransactions(structuredTrx);
        } catch (error) {
            console.log(error);
        }
    }

    const checkIfWalletConnected = async () => {
        if (!ethereum) return alert("Please Install Metamask");
        const accounts = await ethereum.request({
            method: "eth_accounts",
        });
        if (accounts.length) {
            setCurrentAccount(accounts[0]);
            // Get AllTransactions();
            getAllTransactions();
        } else {
            console.log("No accounts found");
        }
        console.log("ACCOUNT", accounts);
    };

    const connectWallet = async () => {
        try {
            if (!ethereum) return alert("Please Install Metamask");
            const accounts = await ethereum.request({
                method: "eth_requestAccounts",
            });
            setCurrentAccount(accounts[0]);
        } catch (error) {
            console.log("No ethereum object.", error);
            throw new Error("No ethereum object.");
        }
    };

    const checkIfTransactionExist = async () => {
        try {
            const trxContarct = getEthereumContract();
            const transactionCount = await trxContarct.getTransactionCount();
            window.localStorage.setItem("transactionCount", transactionCount);
        } catch (error) {

        }
    }

    const sendTransactions = async () => {
        try {
            if (!ethereum) return alert("Please Install Metamask");
            // get the data from the form...
            const { addressTo, amount, keyword, message } = formData;
            const trxContarct = getEthereumContract();
            const parseAmount = ethers.utils.parseEther(amount);

            await ethereum.request({
                method: 'eth_sendTransaction',
                params: [{
                    from: currentAccount,
                    to: addressTo,
                    gas: '0x5208', // 21000 GWEI
                    value: parseAmount._hex, // 0.000011
                }]
            });
            const transactionHash = await trxContarct.addToBlockchain(addressTo, parseAmount, keyword, message);
            setIsLoading(true);
            console.log(`HASH LOADING - ${transactionHash.hash}`);
            await transactionHash.wait();
            setIsLoading(false);
            console.log(`HASH SUCCESS- ${transactionHash.hash}`)
            const transactionCount = await trxContarct.getTransactionCount();
            setTransactionCount(transactionCount.toNumber());
            console.log(`TRANSACTION COUNT - ${transactionCount.toNumber()}`);

        } catch (error) {
            console.log(error)

        }
    }

    useEffect(() => {
        checkIfWalletConnected();
        checkIfTransactionExist();
    }, [transactionCount]);

    return (
        <TransactionContext.Provider value={{
            transactionCount, connectWallet, transactions, currentAccount, isLoading, sendTransactions,
            handleChange, formData

        }}>
            {children}
        </TransactionContext.Provider>
    );
};

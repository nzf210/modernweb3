import { HiMenuAlt4 } from "react-icons/hi";
import { AiOutlineClose } from "react-icons/ai";
import logo from "../../images/logo.png";
import { useState } from "react";

const NavbarItem = ({ title, classProps }) => {
    return <li className={`mx-4 cursor-pointer ${classProps}`}>{title}</li>;
};

const Navbar = () => {
    const [togleMenu, setTogleMenu] = useState(false);
    return (
        <nav className="flex w-full items-center justify-between p-4 md:justify-center">
            <div className="flex-initial items-center justify-center md:flex-[0.5]">
                <img
                    src={logo}
                    alt="logo"
                    className="h-8 w-32 cursor-pointer"
                />
            </div>
            <ul className="hidden flex-initial list-none items-center justify-between text-white md:flex">
                {["Market", "Exchange", "Tutorial", "Wallets"].map((e, i) => (
                    <NavbarItem key={e + i} title={e} classProps={i} />
                ))}
                <li className="mx-4 cursor-pointer rounded-full bg-[#2952e3] py-2 px-7 hover:bg-[#1546bd]">
                    Login
                </li>
            </ul>
            <div className="relative flex">
                {togleMenu ? (
                    <AiOutlineClose
                        fontSize={28}
                        className="cursor-pointer text-white md:hidden"
                        onClick={() => setTogleMenu(false)}
                    />
                ) : (
                    <HiMenuAlt4
                        fontSize={28}
                        className="cursor-pointer text-white md:hidden"
                        onClick={() => setTogleMenu(true)}
                    />
                )}
                {togleMenu && (
                    <ul
                        className="blue-glassmorphism fixed top-0 -right-2 z-10 flex h-screen w-[70vw] animate-slide-in list-none
                    flex-col items-end justify-start rounded-md p-3 text-white shadow-2xl md:hidden"
                    >
                        <li className="my-2 w-full text-xl">
                            <AiOutlineClose
                                onClick={() => setTogleMenu(false)}
                            />
                        </li>
                        {["Market", "Exchange", "Tutorial", "Wallets"].map(
                            (e, i) => (
                                <NavbarItem
                                    key={e + i}
                                    title={e}
                                    classProps="my-2 text-lg"
                                />
                            )
                        )}
                    </ul>
                )}
            </div>
        </nav>
    );
};

export default Navbar;
